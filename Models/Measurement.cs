﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TestBeehive.Models
{
    public class Measurement
    {
        [Key]
        public int ID { get; set; }

        [Required]
        public DateTime DT { get; set; }

        [Required]
        public double Weight { get; set; }

        [Required]
        public double Current { get; set; }

        [Required]
        [ForeignKey("HiveID")]
        public int HiveID { get; set; }

        public Measurement()
        {
            DT = DateTime.Now;
        }

    }
}
