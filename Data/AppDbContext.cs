﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestBeehive.Models;

namespace TestBeehive.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {

        }

        //Sets for DB
        public DbSet<Measurement> Measurements { get; set; }
        public DbSet<Hive> Hives { get; set; }
    }
}
