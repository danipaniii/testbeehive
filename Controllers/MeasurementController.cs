﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TestBeehive.Data;
using TestBeehive.Models;

namespace TestBeehive.Controllers
{
    public class MeasurementController :  Controller
    {
        private readonly AppDbContext _db;

        public MeasurementController(AppDbContext db)
        {
            _db = db;
        }

        //GET - Index View
        public IActionResult Index()
        {
            IEnumerable<Measurement> objects = _db.Measurements;

            return View(objects);
        }

        //GET - Create View
        public IActionResult Create()
        {
            return View();
        }

        //POST - Create Measurement
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Measurement obj)
        {
            _db.Measurements.Add(obj);
            _db.SaveChanges();

            return RedirectToAction("Index"); //Back to Index View
        }
    }
}
